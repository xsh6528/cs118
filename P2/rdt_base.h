//
// Created by Shihao Xu on 3/9/17.
//

#ifndef CS118_RDT_BASE_H
#define CS118_RDT_BASE_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <string.h>
#include <vector>
#include <map>
#include <string>
#include <ctime>
#include <iostream>
#include <vector>
#include <limits>
#include <fstream>
#include "segment.h"
#include "send_buffer.h"
#include "recv_buffer.h"
#include <stdint.h>

const uint16_t HEADER_LENGTH = 5;
const uint16_t MAX_SEG_SIZE = 1024;
const uint16_t MAX_SEQ_NUM = 30720;

const int64_t RETRANS_TIMEOUT_MSEC = 500;
const int64_t RETRY_TIMEOUT_USEC = 900000;
const int64_t FIN_TIME_WAIT = 2 * RETRANS_TIMEOUT_MSEC;
const int MAX_RETRY_TIMES = 60;

using namespace std;

class RecvBuffer;
class SendBuffer;

class ReliableDataTransfer {
public:
    enum State {
        CLOSED,
        ESTABLISHED,

        SYN_SENT,
        CLOSE_WAIT,
        LAST_ACK,

        LISTEN,
        SYN_RCVD,
        FIN_WAIT_1,
        FIN_WAIT_2,
        TIME_WAIT,
    };
    ReliableDataTransfer(short my_port = 0);
    ~ReliableDataTransfer();

    void reset();
    ssize_t send_seg(const string &serialized_seg);
    ssize_t recv_seg(Segment &seg, bool is_save_addr = false);
    ssize_t send_ack(uint16_t seq_num);
protected:
    int select(struct timeval &tv);

    struct sockaddr_in remote_address_;
    int socket_fd_;
    fd_set read_fds_;
    int retry_cnt_;
    State state_;

    uint16_t get_next_seq_num();
    uint16_t get_next_ack_num();
    uint16_t next_ack_num_;

    RecvBuffer *recv_buf_ptr_;

    friend class SendBuffer;
    SendBuffer *send_buf_ptr_;
};

#endif //CS118_RDT_BASE_H
