//
// Created by Shihao Xu on 3/9/17.
//

#include "rdt_client.h"

int main(int argc, char **argv) {
    string server_host = "127.0.0.1";
    short server_port = 3000;
    string file_name = "send.data";

    if (argc >= 2) {
        server_host = argv[1];
    }
    if (argc >= 3) {
        server_port = static_cast<short>(atoi(argv[2]));
    }
    if (argc >= 4) {
        file_name = argv[3];
    }

    RDTClient rdt;

    // three-way handshake
    if (rdt.connect(server_host, server_port, file_name) < 0) {
        perror("connect failed");
        exit(-1);
    }

    // specify filename

    // loop to receive file byte stream,
    // append file
    // if recvlen == 0, means connection CLOSED, break:

    // close file
    // remove corrupted file
    // intact file

    return 0;
}
