//
// Created by Shihao Xu on 3/12/17.
//

#ifndef CS118_RECV_BUFFER_H
#define CS118_RECV_BUFFER_H

#include "rdt_base.h"

class ReliableDataTransfer;

class RecvBuffer {
public:
    map<uint16_t, Segment> buf_;
    uint16_t base_;
    uint16_t wnd_;  // payload window size

    void push(ofstream &ofs, Segment &seg);
    void clear();
};

#endif //CS118_RECV_BUFFER_H
