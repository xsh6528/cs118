//
// Created by Shihao Xu on 3/9/17.
//

#include "segment.h"

Segment::Segment() {
    seq_num_ = 0;
    ack_num_ = 0;
    is_ack_ = 0;
    is_syn_ = 0;
    is_fin_ = 0;
}

Segment::Segment(uint16_t seq_num, uint16_t ack_num, bool is_ack, bool is_syn, bool is_fin) {
    seq_num_ = seq_num;
    ack_num_ = ack_num;
    is_ack_ = is_ack;
    is_syn_ = is_syn;
    is_fin_ = is_fin;
}

Segment::Segment(string payload, uint16_t seq_num, uint16_t ack_num, bool is_ack, bool is_syn, bool is_fin) {
    seq_num_ = seq_num;
    ack_num_ = ack_num;
    is_ack_ = is_ack;
    is_syn_ = is_syn;
    is_fin_ = is_fin;
    payload_ = payload;
}

uint16_t Segment::getSeqNum() {
    return seq_num_;
}

uint16_t Segment::getAckNum() {
    return ack_num_;
}

bool Segment::getAck() {
    return is_ack_;
}

bool Segment::getSyn() {
    return is_syn_;
}

bool Segment::getFin() {
    return is_fin_;
}

string Segment::getPayload() {
    return payload_;
}

uint16_t Segment::getPayloadSize() {
    return payload_.size();
}

string Segment::serialize() {
    string encoded_header;
    encoded_header.push_back(seq_num_ >> 8);
    encoded_header.push_back(seq_num_);
    encoded_header.push_back(ack_num_ >> 8);
    encoded_header.push_back(ack_num_);
    encoded_header.push_back((is_ack_ << 2) + (is_syn_ << 1) + is_fin_);
    return encoded_header + payload_;
}

void Segment::deserialize(string &serialized_seg) {
    unsigned char high, low;

    high = serialized_seg[0];
    low = serialized_seg[1];
    seq_num_ = (high << 8) + low;

    high = serialized_seg[2];
    low = serialized_seg[3];
    ack_num_ = (high << 8) + low;

    low = serialized_seg[4];

    is_ack_ = low & 4;
    is_syn_ = low & 2;
    is_fin_ = low & 1;

    payload_ = serialized_seg.substr(5);
}
