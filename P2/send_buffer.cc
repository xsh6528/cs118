//
// Created by Shihao Xu on 3/11/17.
//

#include "send_buffer.h"

bool SendBuffer::empty() {
    return buf_.empty();
}

uint16_t SendBuffer::available() {
    if (end_ >= base_) {
        return wnd_ - (end_ - base_);
    } else {
        return wnd_ - (end_ + MAX_SEQ_NUM - base_);
    }
}

void SendBuffer::push(ReliableDataTransfer *rdt, Segment &seg) {
    // send_seg segment
    ssize_t sent_len;
    if ((sent_len = rdt->send_seg(seg.serialize())) < 0) {
        perror("send_seg failed");
    }

    // get its timestamp
    struct timeval tv = {0, 0};
    gettimeofday(&tv, NULL);
    int64_t curr_time = static_cast<int64_t>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;

    // update window
    buf_.insert(make_pair(curr_time, seg));
    end_ = (end_ + static_cast<uint16_t >(seg.getPayloadSize()) ) % MAX_SEQ_NUM;
}

void SendBuffer::clear() {
    base_ = end_;
    buf_.clear();
}

Segment &SendBuffer::front_seg() {
    return buf_.begin()->second;
}

int SendBuffer::send_timeout_segs(ReliableDataTransfer *rdt) {
    int retransmit_cnt = 0;  // 0 for first time send_seg, 1 for retransmission

    struct timeval tv = {0, 0};
    int64_t curr_time;

    while (!buf_.empty()) {
        // find out timeout threshold
        gettimeofday(&tv, NULL);
        curr_time = static_cast<int64_t>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;
        int64_t timeout_timestamp = curr_time - RETRANS_TIMEOUT_MSEC;

        // whether there is timeout segment
        int64_t oldest_timestamp = buf_.begin()->first;
        if (oldest_timestamp > timeout_timestamp) {
            break;
        }

        // print
        cout << "Sending packet " << front_seg().getSeqNum() << " 5120" << " Retransmission" << endl;
        retransmit_cnt += 1;

        // send_seg timeout segment
        ssize_t sent_len;
        if ((sent_len = rdt->send_seg(front_seg().serialize())) < 0) {
            perror("send_seg failed");
            continue;
        }

        // update its timestamp
        gettimeofday(&tv, NULL);
        curr_time = static_cast<int64_t>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;
        buf_.insert(make_pair(curr_time, front_seg()));
        buf_.erase(buf_.begin());
    }
    return retransmit_cnt;
}

void SendBuffer::send_one_seg(ReliableDataTransfer *rdt) {
    ssize_t sent_len;
    if ((sent_len = rdt->send_seg(front_seg().serialize())) < 0) {
        perror("send_seg failed");
    }
};

void SendBuffer::label_ack(uint16_t ack_num) {
    // find the segment that has the Seq number == the incoming ACK number
    for (map<int64_t, Segment>::iterator iter = buf_.begin(); iter != buf_.end(); ++iter) {
        if (iter->second.getSeqNum() == ack_num) {
            // label as ACKed, i.e. erase from buf_
            buf_.erase(iter);
            break;
        }
    }

    // update send_base
    if (buf_.empty()) {
        base_ = end_;
    } else {
        uint16_t min_seq_num = MAX_SEQ_NUM;
        uint16_t max_seq_num = 0;
        for (map<int64_t, Segment>::iterator iter = buf_.begin(); iter != buf_.end(); ++iter) {
            uint16_t seq_num = iter->second.getSeqNum();
            min_seq_num = min(seq_num, min_seq_num);
            max_seq_num = max(seq_num, max_seq_num);
        }

        // window wrapped
        if (max_seq_num - min_seq_num > wnd_) {
            min_seq_num = MAX_SEQ_NUM;
            for (map<int64_t, Segment>::iterator iter = buf_.begin(); iter != buf_.end(); ++iter) {
                uint16_t seq_num = iter->second.getSeqNum();
                if (seq_num >= wnd_) {
                    min_seq_num = min(seq_num, min_seq_num);
                }
            }
        }
        base_ = min_seq_num;
    }
}
