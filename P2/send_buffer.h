//
// Created by Shihao Xu on 3/11/17.
//

#ifndef CS118_SEND_BUFFER_H
#define CS118_SEND_BUFFER_H

#include "rdt_base.h"

class ReliableDataTransfer;

class SendBuffer {
public:
    multimap<int64_t, Segment> buf_;
    uint16_t base_;
    uint16_t end_;
    uint16_t wnd_;  // payload window size

    bool empty();
    uint16_t available();
    void push(ReliableDataTransfer *rdt, Segment &seg);
    void clear();
    Segment &front_seg();

    void send_one_seg(ReliableDataTransfer *rdt);
    int send_timeout_segs(ReliableDataTransfer *rdt);
    void label_ack(uint16_t ack_num);
};

#endif //CS118_SEND_BUFFER_H
