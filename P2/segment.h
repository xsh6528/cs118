//
// Created by Shihao Xu on 3/9/17.
//

#ifndef CS118_SEGMENT_H
#define CS118_SEGMENT_H

#include <string>
#include <stdint.h>

using namespace std;

class Segment {
public:
    Segment();
    Segment(uint16_t seq_num, uint16_t ack_num = 0, bool is_ack = 0, bool is_syn = 0, bool is_fin = 0);
    Segment(string payload, uint16_t seq_num, uint16_t ack_num = 0, bool is_ack = 0, bool is_syn = 0, bool is_fin = 0);
    uint16_t getSeqNum();
    uint16_t getAckNum();
    bool getAck();
    bool getSyn();
    bool getFin();
    string getPayload();
    uint16_t getPayloadSize();
    string serialize();
    void deserialize(string &serialized_seg);
private:
    uint16_t seq_num_;
    uint16_t ack_num_;
    bool is_ack_;
    bool is_syn_;
    bool is_fin_;
    string payload_;
};

#endif //CS118_SEGMENT_H
