//
// Created by Shihao Xu on 3/9/17.
//

#include "rdt_client.h"

RDTClient::RDTClient(short my_port) : ReliableDataTransfer(my_port) {}

int RDTClient::connect(string remote_IP, int remote_port, string file_name) {
    // save the server's address to remote_address_ */
    memset((char *) &remote_address_, 0, sizeof(remote_address_));
    remote_address_.sin_family = AF_INET;
    remote_address_.sin_port = htons(remote_port);
    if (inet_pton(AF_INET, remote_IP.c_str(), &(remote_address_.sin_addr)) <= 0) {
        fprintf(stderr, "inet_aton() failed\n");
        return -1;
    }

    file_name_ = file_name;

    // three-way handshake
    return run();
}

int RDTClient::run() {
    reset();  // make sure it is at CLOSED state

    int64_t timeout_event_time = 0;
    int64_t time_wait_start_time = 0;
    int64_t curr_time;
    struct timeval tv = {0, 0};

    // create file
    string filename("received.data");
    ofs_.close();
    ofs_.open(filename.c_str(), ios::out | ios::binary);
    if (!ofs_.is_open()) {
        perror("open file failed");
        return -1;
    }

    for (;;) {
        int return_code = 0;
        switch (state_) {
            case SYN_SENT: return_code = runSynSent(); break;
            case ESTABLISHED: return_code = runEstablished(); break;
            case TIME_WAIT: return_code = runTimeWait(&time_wait_start_time); break;
            default: state_ = SYN_SENT; break;
        }
        if (return_code < 0) {
            return return_code;
        }
        else if (return_code == 0) {
            retry_cnt_ = 0;
            timeout_event_time = 0;
        }
        else if (return_code == 1) {  // No Response
            gettimeofday(&tv, NULL);
            curr_time = static_cast<int64_t>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;

            if (curr_time - timeout_event_time > RETRANS_TIMEOUT_MSEC) {
                if (timeout_event_time > 0) {
                    retry_cnt_ += 1;
                    cout << "Retry: " << retry_cnt_ << endl;
                    if (retry_cnt_ >= MAX_RETRY_TIMES) {
                        // delete file
                        std::remove(filename.c_str());

                        reset();
                        cout << "Retry limit exceeded. Reset ..." << endl;
                        return 1;
                    }
                }
                timeout_event_time = curr_time;
            }
        } else if (return_code == 2) {  // Finish
            ofs_.close();
            return 0;
        } else if (return_code == 3) {
            ofs_.close();
            cout << "Closed. File not exits" << endl;
            return 0;
        }
    }
}

int RDTClient::runSynSent() {
    if (send_buf_ptr_->empty()) {
        // make segment
        string payload = file_name_;
        Segment syn_seg(payload, get_next_seq_num(), 0, 0, 1);

        send_buf_ptr_->push(this, syn_seg);

        cout << "Sending packet SYN" << std::endl;
    } else {
        // retransmit segment
        send_buf_ptr_->send_one_seg(this);
        cout << "Sending packet " << send_buf_ptr_->front_seg().getSeqNum() << " SYN Retransmission" << std::endl;
    }
//    cout << "Sending packet Seq=" << send_buf_ptr_->front_seg().getSeqNum() << " SYN" << std::endl;

    // wait
    struct timeval tv = {0, 0};
    tv.tv_usec = RETRY_TIMEOUT_USEC;
    int nReadyFds = select(tv);

    // timeout
    if (nReadyFds == 0) {
        return 1;  // retry waiting for SYN+ACK (three-way handshake)
    }

    // receive segment
    ssize_t recv_len;
    Segment seg;
    if ((recv_len = recv_seg(seg)) < 0) {
        return -1;
    }

    cout << "Receiving packet " << seg.getSeqNum() << endl;

    // test state transfer conditions
    if (seg.getAck() && seg.getAckNum() == send_buf_ptr_->front_seg().getSeqNum()) {
        //cout << " SYN+ACK" << endl;

        if (!seg.getSyn()) {
            state_ = CLOSED;
            return 3;
        }

        send_ack(seg.getSeqNum());

        recv_buf_ptr_->base_ = (seg.getSeqNum() + seg.getPayloadSize()) % MAX_SEQ_NUM;
        send_buf_ptr_->clear();

        state_ = ESTABLISHED;
        cout << "Established ..." << endl;
    } else {
        cout << endl;
    }
    return 0;
}

int RDTClient::runEstablished() {
    struct timeval tv = {0, 0};
    int nReadyFds = select(tv);

    // no segment to receive
    if (nReadyFds == 0) {
        return 1;
    }

    // receive segment
    ssize_t recv_len;
    Segment seg;
    if ((recv_len = recv_seg(seg)) < 0) {
        return -1;
    }

    cout << "Receiving packet " << seg.getSeqNum() << endl;

    // ACK
    send_ack(seg.getSeqNum());

    // test state transfer conditions
    if (seg.getFin()) {
        recv_buf_ptr_->clear();
        state_ = TIME_WAIT;
        cout << "Time Wait ..." << endl;
        return 0;
    }

    // add to window
    recv_buf_ptr_->push(ofs_, seg);

    return 0;
}

int RDTClient::runTimeWait(int64_t *time_wait_start_time_ptr) {
    int64_t &timewait_start_time = *time_wait_start_time_ptr;

    int64_t curr_time;
    struct timeval tv = {0, 0};

    gettimeofday(&tv, NULL);
    curr_time = static_cast<int64_t>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;

    if (!timewait_start_time) {
        timewait_start_time = curr_time;
    }

    if (curr_time - timewait_start_time > 6 * RETRANS_TIMEOUT_MSEC) {
        state_ = CLOSED;
        cout << "Closed ..." << endl;
        return 2;
    }

    tv.tv_sec = 0;
    tv.tv_usec = 0;
    int nReadyFds = select(tv);

    // no segment to receive
    if (nReadyFds == 0) {
        return 0;
    }

    // receive segment
    ssize_t recv_len;
    Segment seg;
    if ((recv_len = recv_seg(seg)) < 0) {
        return -1;
    }

    cout << "Receiving packet " << seg.getSeqNum() << endl;

    // ACK
    send_ack(seg.getSeqNum());

    return 0;
}
