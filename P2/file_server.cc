//
// Created by Shihao Xu on 3/3/17.
//

#include "rdt_server.h"

int main(int argc, char **argv) {
    short my_port = 3000;
    if (argc >= 2) {
        my_port = static_cast<short>(atoi(argv[1]));
    }

    RDTServer rdt(my_port);
    for(;;) {
        if (rdt.run() < 0) {
            exit(-1);
        }
    }
}
