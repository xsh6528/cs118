//
// Created by Shihao Xu on 3/9/17.
//

#ifndef CS118_RDT_SERVER_H
#define CS118_RDT_SERVER_H

#include "rdt_base.h"

class RDTServer : public ReliableDataTransfer {
public:
    RDTServer(short my_port);
    virtual ~RDTServer();
    int run();
private:
    int runListen();
    int runSynRcvd();
    int runEstablished();
    int runLastAck();

    ifstream ifs_;
};

#endif //CS118_RDT_SERVER_H
