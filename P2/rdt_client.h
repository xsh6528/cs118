//
// Created by Shihao Xu on 3/9/17.
//

#ifndef CS118_RDT_CLIENT_H
#define CS118_RDT_CLIENT_H

#include "rdt_base.h"

class RDTClient : public ReliableDataTransfer {
public:
    RDTClient(short my_port = 0);
    int connect(string remote_IP, int remote_port, string filename);  // three-way handshake
private:
    int run();
    int runSynSent();
    int runEstablished();
    int runTimeWait(int64_t *time_wait_start_time_ptr);

    int sendSyn();
    int sendAck(uint16_t seq_num);

    RecvBuffer *recv_buf_;
    ofstream ofs_;

    string file_name_;
};

#endif //CS118_RDT_CLIENT_H
