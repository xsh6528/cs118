//
// Created by Shihao Xu on 3/12/17.
//

#include "recv_buffer.h"

void RecvBuffer::clear() {
    buf_.clear();
}

void RecvBuffer::push(ofstream &ofs, Segment &seg) {
    // only push expected seg
    uint16_t left = base_, right = (base_ + wnd_) % MAX_SEQ_NUM;
    uint16_t seq_num = seg.getSeqNum();

    if (left <= right) {
        if (seq_num < left || seq_num >= right) {
            return;
        }
    } else {
        if (seq_num < left && seq_num >= right) {
            return;
        }
    }

    buf_.insert(make_pair(seq_num, seg));

    while (buf_.count(base_)) {
        // append to file
        Segment seg = buf_[base_];
        buf_.erase(base_);

        ofs << seg.getPayload();
        base_ = (base_ + seg.getPayloadSize()) % MAX_SEQ_NUM;
    }
}
