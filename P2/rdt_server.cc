//
// Created by Shihao Xu on 3/9/17.
//

#include "rdt_server.h"

RDTServer::RDTServer(short my_port) : ReliableDataTransfer(my_port) {}

RDTServer::~RDTServer() {}

int RDTServer::run() {
    reset();  // make sure it is at CLOSED state

    int64_t timeout_event_time = 0;
    int64_t curr_time;
    struct timeval tv = {0, 0};

    for (;;) {
        int return_code = 0;
        switch (state_) {
            case LISTEN: return_code = runListen(); break;
            case SYN_RCVD: return_code = runSynRcvd(); break;
            case ESTABLISHED: return_code = runEstablished(); break;
            case LAST_ACK: return_code = runLastAck(); break;
            default: state_ = LISTEN; break;
        }
        if (return_code < 0) {
            return return_code;
        }
        else if (return_code == 0) {
            retry_cnt_ = 0;
            timeout_event_time = 0;
        }
        else if (return_code == 1) {
            gettimeofday(&tv, NULL);
            curr_time = static_cast<int64_t>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;

            if (curr_time - timeout_event_time > RETRANS_TIMEOUT_MSEC) {
                if (timeout_event_time > 0) {
                    retry_cnt_ += 1;
                    cout << "Retry: " << retry_cnt_ << endl;
                    if (retry_cnt_ >= MAX_RETRY_TIMES) {
                        reset();
                        cout << "Retry limit exceeded. Reset ..." << endl;
                        return 1;
                    }
                }
                timeout_event_time = curr_time;
            }
        } else if (return_code == 2) {
            return 0;
        }
    }
}

int RDTServer::runListen() {
    // wait
    struct timeval tv = {0, 0};
    tv.tv_usec = RETRY_TIMEOUT_USEC;
    int nReadyFds = select(tv);

    // timeout
    if (nReadyFds == 0) {
        return 0;
    }

    // receive segment
    ssize_t recv_len;
    Segment seg;
    if ((recv_len = recv_seg(seg, true)) < 0) {
        return -1;
    }

    // SYN packet from client
    cout << "Receiving packet " << seg.getAckNum() << endl;

    // test transfer condition
    if (seg.getSyn()) {
        state_ = SYN_RCVD;
        next_ack_num_ = seg.getSeqNum();
        ifs_.close();
        ifs_.open(seg.getPayload().c_str(), ios::in | ios::binary);
        cout << "SYN_RCVD ..." << endl;
    }

    return 0;
}

int RDTServer::runSynRcvd() {
    if (send_buf_ptr_->empty()) {
        // make segment
        string payload = "x";

        if (ifs_.is_open()) {
            Segment syn_ack_seg(payload, get_next_seq_num(), next_ack_num_, 1, 1);
            send_buf_ptr_->push(this, syn_ack_seg);
        } else {
            Segment syn_ack_seg(payload, get_next_seq_num(), next_ack_num_, 1, 0);
            send_buf_ptr_->push(this, syn_ack_seg);

            cout << "Closed. File not exits ..." << endl;
            state_ = CLOSED;
            return 2;
        }
    } else {
        // retransmit segment
        send_buf_ptr_->send_one_seg(this);
    }
    cout << "Sending packet " << send_buf_ptr_->front_seg().getSeqNum() << " 5120"
         << " SYN" << std::endl;

    // wait
    struct timeval tv = {0, 0};
    tv.tv_usec = RETRY_TIMEOUT_USEC;
    int nReadyFds = select(tv);

    // timeout
    if (nReadyFds == 0) {
        return 1;  // retry waiting for ACK (three-way handshake)
    }

    // receive segment
    ssize_t recv_len;
    Segment seg;
    if ((recv_len = recv_seg(seg)) < 0) {
        return -1;
    }

    cout << "Receiving packet " << seg.getAckNum() << endl;

    // test state transfer condition
    if (seg.getAck() && seg.getAckNum() == send_buf_ptr_->front_seg().getSeqNum()) {
        send_buf_ptr_->clear();
        state_ = ESTABLISHED;
        cout << "Established ..." << endl;
    }
    return 0;
}

int RDTServer::runEstablished() {
    // make segment
    while (true) {
        // decide max payload size and read file to get actual payload size
        string payload;

        uint16_t allowed_payload_size = min(send_buf_ptr_->available(),
                                            static_cast<uint16_t>(MAX_SEG_SIZE - HEADER_LENGTH));

        payload.resize(allowed_payload_size);
        ifs_.read(&payload[0], allowed_payload_size);
        payload.resize(ifs_.gcount());

        if (!payload.size()) {
            break;
        }

        Segment seg(payload, get_next_seq_num());

        // add into window
        send_buf_ptr_->push(this, seg);

        cout << "Sending packet " << seg.getSeqNum() << " 5120" << endl;
    }
    if (send_buf_ptr_->empty()) {
        state_ = LAST_ACK;
    }

    // retransmit segment
    send_buf_ptr_->send_timeout_segs(this);

    // receive ACK
    int return_code = 1;
    struct timeval tv = {0, 0};
    while (select(tv) > 0) {
        ssize_t recv_len;
        Segment seg;
        if ((recv_len = recv_seg(seg)) < 0) {
            return -1;
        }

        if (seg.getAck()) {
            cout << "Receiving packet " << seg.getAckNum() << endl;
            send_buf_ptr_->label_ack(seg.getAckNum());
            return_code = 0;
        }
    }

    return return_code;
}

int RDTServer::runLastAck() {
    if (send_buf_ptr_->empty()) {
        // make segment
        string payload = "x";
        Segment syn_ack_seg(payload, get_next_seq_num(), 0, 0, 0, 1);

        send_buf_ptr_->push(this, syn_ack_seg);
        cout << "Sending packet " << send_buf_ptr_->front_seg().getSeqNum() << " 5120"
             << " FIN" << std::endl;
    } else {
        // retransmit segment
        send_buf_ptr_->send_one_seg(this);
        cout << "Sending packet " << send_buf_ptr_->front_seg().getSeqNum() << " 5120" << " Retransmission"
             << " FIN" << std::endl;
    }
//    cout << "Sending packet " << send_buf_ptr_->front_seg().getSeqNum() << " 5120"
//          << " FIN" << std::endl;

    // wait
    struct timeval tv = {0, 0};
    tv.tv_usec = RETRY_TIMEOUT_USEC;
    int nReadyFds = select(tv);

    // timeout
    if (nReadyFds == 0) {
        return 1;
    }

    // receive segment
    ssize_t recv_len;
    Segment seg;
    if ((recv_len = recv_seg(seg)) < 0) {
        return -1;
    }

    cout << "Receiving packet " << seg.getAckNum() << endl;

    // test state transfer condition
    if (seg.getAck() && seg.getAckNum() == send_buf_ptr_->front_seg().getSeqNum()) {
        send_buf_ptr_->clear();
        state_ = CLOSED;
        cout << "Closed ..." << endl;
    }
    return 2;
}
