//
// Created by Shihao Xu on 3/9/17.
//

#include "rdt_base.h"

ReliableDataTransfer::ReliableDataTransfer(short my_port) {
    // create a UDP socket
    if ((socket_fd_ = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("cannot create socket\n");
        exit(-1);
    }

    // Eliminates "Address already in use" error from bind
    int optval = 1;
    if (setsockopt(socket_fd_, SOL_SOCKET, SO_REUSEADDR,
                   (const void *) &optval, sizeof(int)) < 0) {
        perror("ERROR setsockopt");
    }

    // bind the socket to any valid IP address and a specific port
    struct sockaddr_in my_address;
    memset((char *)&my_address, 0, sizeof(my_address));
    my_address.sin_family = AF_INET;
    my_address.sin_addr.s_addr = htonl(INADDR_ANY);
    my_address.sin_port = htons(my_port);

    if (::bind(socket_fd_, (struct sockaddr *)&my_address, sizeof(my_address)) < 0) {
        perror("bind failed");
        exit(-1);
    }

    send_buf_ptr_ = NULL;
    recv_buf_ptr_ = NULL;
}

void ReliableDataTransfer::reset() {
    // initialize state variables
    if (recv_buf_ptr_) {
        delete recv_buf_ptr_;
    }
    recv_buf_ptr_ = new RecvBuffer;

    recv_buf_ptr_->wnd_ = 5095;

    if (send_buf_ptr_) {
        delete send_buf_ptr_;
    }
    send_buf_ptr_ = new SendBuffer;

    srand((unsigned int)time(0));
    send_buf_ptr_->base_ = send_buf_ptr_->end_ = (uint16_t)(rand() % MAX_SEQ_NUM);
    next_ack_num_ = MAX_SEQ_NUM;
    send_buf_ptr_->wnd_ = 5095;
    state_ = CLOSED;
    retry_cnt_ = 0;
}

ReliableDataTransfer::~ReliableDataTransfer() {
    close(socket_fd_);
    if (send_buf_ptr_) {
        delete send_buf_ptr_;
    }
    if (recv_buf_ptr_) {
        delete recv_buf_ptr_;
    }
}

uint16_t ReliableDataTransfer::get_next_seq_num() {
    return send_buf_ptr_->end_;
}

uint16_t ReliableDataTransfer::get_next_ack_num() {
    uint16_t ret = next_ack_num_;
    next_ack_num_ = MAX_SEQ_NUM;
    return ret;
}

// unreliable send_seg
ssize_t ReliableDataTransfer::send_seg(const string &serialized_seg) {
    ssize_t sent_len = ::sendto(socket_fd_, serialized_seg.c_str(), serialized_seg.size(),
                             0, (struct sockaddr *)&remote_address_, sizeof(remote_address_));
    return sent_len;
}

// unreliable recv
ssize_t ReliableDataTransfer::recv_seg(Segment &seg, bool is_save_addr) {
    char buf[MAX_SEG_SIZE];
    struct sockaddr_in their_address;
    socklen_t addr_len = sizeof(their_address);

    ssize_t recv_len = recvfrom(socket_fd_, buf, MAX_SEG_SIZE,
                                0, (struct sockaddr *)&their_address, &addr_len);

    if (is_save_addr) {
        memset((char *)&remote_address_, 0, sizeof(remote_address_));
        remote_address_ = their_address;
    }

    if (recv_len > 0) {
        string serialized_seg(buf, recv_len);
        seg.deserialize(serialized_seg);
    }

    return recv_len;
}

int ReliableDataTransfer::select(struct timeval &tv) {
    FD_SET(socket_fd_, &read_fds_);
    int nReadyFds = ::select(socket_fd_ + 1, &read_fds_, NULL, NULL, &tv);
    if (nReadyFds <= 0) {
        return 0;
    } else {
        return nReadyFds;
    }
}

ssize_t ReliableDataTransfer::send_ack(uint16_t seq_num) {
    string payload;
    Segment ack_seg(get_next_seq_num(), seq_num, 1, 0, 0);

    ssize_t sent_len;
    if ((sent_len = send_seg(ack_seg.serialize())) < 0) {
        perror("send_seg failed");
        return sent_len;
    }
    cout << "Sending packet " << ack_seg.getAckNum()  << std::endl;

    return sent_len;
}
