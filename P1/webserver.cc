//
// Created by Shihao Xu on 2/1/17.
//

#include <sys/types.h>   // definitions of a number of data types used in socket.h and netinet/in.h
#include <sys/socket.h>  // definitions of structures needed for sockets, e.g. sockaddr
#include <netinet/in.h>  // constants and structures needed for internet domain addresses, e.g. sockaddr_in
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <streambuf>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <limits>
#include "webserver.h"

int main(int argc, char *argv[]) {
    uint16_t port = 8080;
    if (argc == 2) {
//        error("ERROR, no port provided");
        port = static_cast<uint16_t>(atoi(argv[1]));
    }

    ListeningSocket listen_socket(port);
    while (true) {
        listen_socket.wait_and_process();
    }

    return 0;
}
