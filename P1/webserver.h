//
// Created by Shihao Xu on 2/1/17.
//

#ifndef CS118_WEBSERVER_H
#define CS118_WEBSERVER_H

#include <sys/types.h>   // definitions of a number of data types used in socket.h and netinet/in.h
#include <sys/socket.h>  // definitions of structures needed for sockets, e.g. sockaddr
#include <netinet/in.h>  // constants and structures needed for internet domain addresses, e.g. sockaddr_in
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <streambuf>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <limits>

void error(std::string msg) {
    perror(msg.c_str());
    exit(-1);
}

class socketbuf : public std::streambuf {
private:
    int socket_;
    size_t buf_size_;
public:
    socketbuf() {}
    socketbuf(int fd, size_t buf_size = 255) {
        socket_ = fd;
        buf_size_ = buf_size;

        char *buf;
        // set get buffer
        // eback()，gptr()，egptr()
        buf = new char[buf_size_];
        setg(buf, buf, buf);

        // set put buffer
        // pbase()，pptr()，epptr()
        buf = new char[buf_size_];
        setp(buf, buf + buf_size_);
    }
    virtual ~socketbuf() {
        delete[] eback();
        delete[] pbase();
    }
    virtual traits_type::int_type underflow() {
        // eback()，gptr()，egptr()
        ssize_t ret = read(socket_, eback(), buf_size_);
        if (ret > 0) {
            setg(eback(), eback(), eback() + ret);
            return traits_type::to_int_type(*gptr());
        } else {
            // ret == 0 || ret == SOCKET_ERROR
            return traits_type::eof();
        }
    }
    virtual traits_type::int_type sync() {  // flush
        // pbase()，pptr()，epptr()
        size_t sent = 0, all = pptr() - pbase();
        while (sent < all) {
            ssize_t ret = write(socket_, pbase() + sent, all - sent);
            setp(pbase(), epptr());
            if (ret > 0) {
                sent += ret;
            } else {
                return -1;
            }
        }
        return 0;
    }
    virtual traits_type::int_type overflow(traits_type::int_type c = traits_type::eof()) {
        if (sync() == -1) {
            return traits_type::eof();
        } else {
            if (!traits_type::eq_int_type(c, traits_type::eof())) {
                sputc(traits_type::to_char_type(c));
            }
            return traits_type::not_eof(c);
        }
    }
};

class Request {
private:
    std::iostream *socket_stream_ptr_;
public:
    std::string uri;
    Request(std::iostream *socket_stream_ptr) {
        socket_stream_ptr_ = socket_stream_ptr;
    }
    void recv() {
        std::iostream &socket_stream = *socket_stream_ptr_;

        /*  Example:
            GET /somedir/page.html HTTP/1.1
            Host: www.someschool.edu
            Connection: close
            User-agent: Mozilla/5.0 Accept-language: fr
        */

        std::string req_line;
        while (std::getline(socket_stream, req_line)
               && !(req_line == "\r\n" || req_line == "\r" || req_line == "\n")) {
            if (uri.empty()) {
                std::istringstream req_line_stream(req_line);
                req_line_stream.ignore(std::numeric_limits<std::streamsize>::max(), ' ');
                req_line_stream >> uri;
            }
            // Part 1, Print HTTP Request
            std::cout << req_line << "\n";
        }
        std::cout << std::endl;
    }
};

class Response {
private:
    std::iostream *socket_stream_ptr_;
public:
    Response(std::iostream *socket_stream_ptr) {
        socket_stream_ptr_ = socket_stream_ptr;
    }
    void send(std::string uri) {
        std::iostream &socket_stream = *socket_stream_ptr_;

        // open file as in stream
        std::string file_name = "." + uri;
        if (file_name[file_name.size() - 1] == '/') {  // default file
            file_name += "index.html";
        }

        std::ifstream ifs(file_name.c_str(), std::ios::in);
        struct stat sbuf;
        if (ifs && !stat(file_name.c_str(), &sbuf) && (sbuf.st_mode & S_IFDIR)) {
            ifs.setstate(std::ios_base::failbit);
        }

        if (!ifs) {
            // status line
            socket_stream << "HTTP/1.1 404 Not Found\n";

            // header lines
            socket_stream << "Connection: close\n";
            socket_stream << "Content-Type: text/plain\n";
            socket_stream << "\n";

            // entity body
            socket_stream << "404 Not Found";
        } else {
            // status line
            socket_stream << "HTTP/1.1 200 OK\n";

            // header lines
            socket_stream << "Connection: close\n";
//            socket_stream << "Date: Tue, 09 Aug 2011 15:44:04 GMT\n";
            socket_stream << "Server: CS 118/1.0.0 (Ubuntu)\n";
//            socket_stream << "Last-Modified: Tue, 09 Aug 2011 15:11:03 GMT\n";
//            socket_stream << "Content-Length: 6821\n";
            socket_stream << "Content-Type: " + get_content_type(file_name) + "\n";
            socket_stream << "\n";

            // entity body
            std::copy(std::istreambuf_iterator<char>(ifs),
                      std::istreambuf_iterator<char>(),
                      std::ostreambuf_iterator<char>(socket_stream));
        }

        // flush
        socket_stream << std::flush;
    }
    std::string get_content_type(std::string file_name) {
        size_t idx;
        if((idx = file_name.find_last_of('.')) != std::string::npos) {
            std::string extension = file_name.substr(idx + 1);
            if (extension == "html" || extension == "htm") {
                return "text/html";
            } else if (extension == "gif") {
                return "image/gif";
            } else if (extension == "jpg" || extension == "jpeg") {
                return "image/jpeg";
            } else if (extension == "png") {
                return "image/png";
            }
        }
        return "text/plain";
    }
};

class ListeningSocket {
private:
    int listenfd_;
    struct sockaddr_in clientaddr;
    socklen_t clientlen;
public:
    ListeningSocket(uint16_t port) {
        listenfd_ = -1;

        // create a socket, AF_INET for IPv4, SOCK_STREAM for TCP
        listenfd_ = socket(AF_INET, SOCK_STREAM, 0);
        if (listenfd_ < 0) {
            error("ERROR opening socket");
        }
        
        /* Eliminates "Address already in use" error from bind */
        int optval = 1;
        if (setsockopt(listenfd_, SOL_SOCKET, SO_REUSEADDR,
                       (const void *) &optval, sizeof(int)) < 0) {
            error("ERROR setsockopt");
        }

        // fill in address info
        struct sockaddr_in serveraddr;
        memset((char *) &serveraddr, 0, sizeof(serveraddr));  // reset memory
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
        serveraddr.sin_port = htons(port);

        // bind a socket to a local IP address and port number
        if (bind(listenfd_, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) {
            error("ERROR on binding");
        }

        // put the socket into passive state, waiting for connections
        if (listen(listenfd_, 5) < 0) {  // 5 simultaneous connection at most
            error("ERROR on listening");
        }
    }
    virtual ~ListeningSocket() {
        close(listenfd_);
    }
    void wait_and_process() {
        // accept connections
        int connfd = accept(listenfd_, (struct sockaddr *) &clientaddr, &clientlen);
        if (connfd < 0) {
            error("ERROR on accept");
        }

        // process connection
        process(connfd);

        // close connection
        close(connfd);
    }
    void process(int connfd) {
        socketbuf sb(connfd);
        std::iostream socket_stream(&sb);

        Request req(&socket_stream);
        req.recv();

        Response res(&socket_stream);
        res.send(req.uri);
    }
};

#endif //CS118_WEBSERVER_H
